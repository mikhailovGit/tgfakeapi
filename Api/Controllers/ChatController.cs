using Api.Dto;
using Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[Route("chat")]
public class ChatController : ControllerBase
{
    private readonly ChatService _chatService;

    public ChatController(ChatService chatService)
    {
        _chatService = chatService;
    }

    [HttpGet("/list")]
    public async Task<IEnumerable<ChatListItem>> ChatList()
    {
        return await _chatService.GetChatList();
    }
    
    [HttpGet("/messages/{chatId:long}")]
    public async Task<IEnumerable<MessageInfo>> ChatList(long chatId)
    {
        return await _chatService.GetMessageList(chatId);
    }
}