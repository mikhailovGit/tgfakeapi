using System.Text.Json;
using System.Text.Json.Serialization;

public class Rootobject
{
    public string about { get; set; }
    public Chats chats { get; set; }
    // public Left_Chats left_chats { get; set; }
}

public class Chats
{
    public string about { get; set; }
    
    [JsonPropertyName("list")]
    public List[]? ChatList { get; set; }
}

public record List
{
    public string name { get; set; }
    public string type { get; set; }
    public int id { get; set; }
    public Message[] messages { get; set; }
}

// public class Left_Chats
// {
//     public string about { get; set; }
//     public List[] list { get; set; }
// }
public class Message
{
    public int id { get; set; }
    public string type { get; set; }
    public DateTime date { get; set; }
    public string? date_unixtime { get; set; }
    public string? actor { get; set; }
    public string? actor_id { get; set; }
    public string? action { get; set; }
    public string? title { get; set; }
    
    [JsonConverter(typeof(MessageConverter))]
    public string[] text { get; set; }
    // public object[] text_entities { get; set; }
}

class  MessageConverter : JsonConverter<string[]>
{
    public override string[]? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        if (reader.TokenType == JsonTokenType.String)
        {
            return new[] { reader.GetString() ?? string.Empty };
        }

        if (reader.TokenType != JsonTokenType.StartArray)
        {
            return Array.Empty<string>();
        }
        
        reader.Read();
        List<string> messages = new(1000);
        while (reader.TokenType != JsonTokenType.EndArray)
        {
            if (reader.TokenType == JsonTokenType.String)
            {
                messages.Add(reader.GetString() ?? string.Empty);
            }
            reader.Read();
        }

        return messages.ToArray();
    }

    public override void Write(Utf8JsonWriter writer, string[] value, JsonSerializerOptions options)
    {
        writer.WriteStartArray("List");
        foreach(var n in value)
        {
            writer.WriteStringValue(n);
        }
        writer.WriteEndArray();
    }

}