﻿using System.Runtime.CompilerServices;
using System.Text.Json;
using Api.Dto;
using Bogus;
using Microsoft.Extensions.Caching.Memory;

namespace Api.Services;

public class ChatService
{
    private readonly IMemoryCache _cache;

    public ChatService(IMemoryCache cache)
    {
        _cache = cache;
    }

    public async Task<IEnumerable<ChatListItem>> GetChatList()
    {
        return await _cache.GetOrCreateAsync(nameof(GetChatList), async _ =>
        {
            var list = await GetJson();
            return
                list
                    .Select(chat => new ChatListItem
                    {
                        Title = chat.name,
                        ChatId = chat.id,
                        LastMessage = chat.messages.MaxBy(message => message.date)?.text.FirstOrDefault(),
                        UnreadMessagesCount = new Random().Next(1, 100)
                    });
        });
    }

    public async Task<IEnumerable<MessageInfo>> GetMessageList(long chatId)
    {
        var faker = new Faker();
        var list = await GetJson();

        return list
                   .FirstOrDefault(_ => _.id == chatId)
                   ?.messages
                   .Where(message => message.type == "message" && message.text.Any(s => !string.IsNullOrEmpty(s)))
                   .TakeLast(100)
                   .Select(message => new MessageInfo()
                   {
                       id = message.id,
                       action = message.action,
                       actor = message.actor ?? faker.Name.FirstName() ,
                       actor_id = message.actor_id ?? "0",
                       date = message.date,
                       title = message.title,
                       type = message.type,
                       date_unixtime = message.date_unixtime,
                       Text = message.text.FirstOrDefault(s => !string.IsNullOrEmpty(s)) ?? string.Empty
                   }) 
               ?? Array.Empty<MessageInfo>();
        //
        // return await _cache.GetOrCreateAsync(chatId, async _ =>
        // {
        //
        // });
    }

    private async Task<List[]> GetJson([CallerMemberName] string name = nameof(GetJson))
    {
        return await _cache.GetOrCreateAsync(name, async _ =>
        {
            var json = await File.ReadAllTextAsync("result.json");
            return JsonSerializer.Deserialize<Rootobject>(json)?.chats.ChatList ?? Array.Empty<List>();
        });
    }
}