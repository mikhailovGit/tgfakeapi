﻿namespace Api.Dto;

public record ChatListItem
{
    public long ChatId { get; init; }
    public string? Title { get; init; }
    public string? LastMessage { get; init; }
    public int? UnreadMessagesCount { get; init; }
    public string? AvatarUri { get; init; }
};