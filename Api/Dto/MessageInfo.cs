﻿namespace Api.Dto;

public record MessageInfo
{
    public int id { get; set; }
    public string type { get; set; }
    public DateTime date { get; set; }
    public string date_unixtime { get; set; }
    public string actor { get; set; }
    public string actor_id { get; set; }
    public string action { get; set; }
    public string title { get; set; }
    public string Text { get; set; }
}