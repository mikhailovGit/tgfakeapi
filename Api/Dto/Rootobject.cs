﻿namespace tgFakeApi;

public class Rootobject
{
    public string about { get; set; }
    public Chats chats { get; set; }
    public Left_Chats left_chats { get; set; }
}

public class Chats
{
    public string about { get; set; }
    public List[] list { get; set; }
}

public class List
{
    public string name { get; set; }
    public string type { get; set; }
    public int id { get; set; }
    public object[] messages { get; set; }
}

public class Left_Chats
{
    public string about { get; set; }
    public List1[] list { get; set; }
}

public class List1
{
    public string name { get; set; }
    public string type { get; set; }
    public int id { get; set; }
    public object[] messages { get; set; }
}